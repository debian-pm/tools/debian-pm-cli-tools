// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-only

extern crate debian;
use ansi_term::{Style, Colour};
use core::convert::TryFrom;
use debian::package::*;
use std::env;
use std::fs::*;
use std::io::prelude::*;
use std::io::ErrorKind;
use std::path::Path;
use std::process::Command;
extern crate chrono;
use crate::chrono::Datelike;

static DEBHELPER_COMPAT: i8 = 13;

fn init_git_repository(path: &str) {
    let cmd_result = Command::new("git")
        .arg("-C")
        .arg(path)
        .arg("init")
        .status();

    if let Ok(status) = cmd_result {
        if let Some(0) = status.code() {
            return;
        }
    }

    eprintln!("{}: Failed to initialize git repository", Style::new().bold().fg(Colour::Red).paint("Error"));
}

fn add_git_remote(path: &str, name: &str, url: &str) {
    let cmd_result = Command::new("git")
        .arg("-C")
        .arg(path)
        .arg("remote")
        .arg("add")
        .arg(name)
        .arg(url)
        .status();

    if let Ok(status) = cmd_result {
        if let Some(0) = status.code() {
            return;
        }
    }

    eprintln!("{}: Failed to add remote to git repository", Style::new().bold().fg(Colour::Red).paint("Error"));
}

#[derive(Clone)]
struct Maintainer {
    name: String,
    email: String,
}

impl TryFrom<String> for Maintainer {
    type Error = &'static str;

    fn try_from(string: String) -> Result<Self, Self::Error> {
        let tmpstr = string.replace('>', "");
        if tmpstr.contains('<') {
            let parts = tmpstr.split('<').collect::<Vec<&str>>();
            Ok(Self {
                name: parts[0].to_string(),
                email: parts[1].to_string(),
            })
        } else {
            Err("Not in Name <Email> format")
        }
    }
}

impl From<Maintainer> for String {
    fn from(value: Maintainer) -> Self {
        format!("{} <{}>", value.name, value.email)
    }
}

impl Maintainer {
    fn export_to_env(&self) {
        env::set_var("NAME", self.name.clone());
        env::set_var("EMAIL", self.email.clone())
    }
}

/// Waits for input from the user and returns the typed in string
fn ask() -> String {
    let mut input = String::new();
    std::io::stdin().read_line(&mut input).unwrap();
    input = input.replace('\n', "");

    input
}

/// prints text in bold
fn print_bold(text: &str) {
    println!("{}", Style::new().bold().paint(text))
}

/// Asks a given question and returns the input from the user or a given default values
fn ask_input(question: &str, default_value: Option<&str>) -> String {
    match default_value {
        Some(s) => {
            if s.is_empty() {
                println!("{} (Default: empty)", Style::new().bold().paint(question))
            } else {
                println!("{} (Default: {})", Style::new().bold().paint(question), s)
            }
        }
        None => print_bold(question),
    };
    let input = ask();

    if input.is_empty() {
        match default_value {
            Some(s) => s.to_string(),
            None => {
                println!("This question doesn't have a default value, please answer it:");
                ask_input(question, default_value)
            }
        }
    } else {
        input
    }
}

/// Asks a given question and adds the user's input or a default value to a field of a debian::ControlParagraph
fn ask_entry(
    append_pragraph: &mut ControlParagraph,
    field: &str,
    question: &str,
    default_value: Option<&str>,
) {
    append_pragraph.add_entry(field, ask_input(question, default_value))
}

fn ask_name_email() -> Maintainer {
    let name = ask_input(
        "Please enter your name (Set the NAME environment variable to save the value)",
        None,
    );
    let email = ask_input(
        "Please enter your email (Set the EMAIL environment variable to save the value)",
        None,
    );

    Maintainer { name, email }
}

fn create_folders(source_package: &str) {
    match create_dir_all(format!("{source_package}/debian/source/")) {
        Ok(_o) => (),
        Err(e) => match e.kind() {
            ErrorKind::AlreadyExists => (),
            _ => panic!("Unhandled error creating the debian/source directory"),
        },
    }
}

fn generate_watch_file(source_package: &str, url: &str) -> std::io::Result<()> {
    let mut watch_file = File::create(format!("{source_package}/debian/watch"))
        .expect("failed to create watch file");

    writeln!(watch_file, "version=4")?;
    if url.contains("github.com") {
        writeln!(
            watch_file,
            r"opts=filenamemangle=s/.+\/v?(\d\S+)\.tar\.gz/{}-$1\.tar\.gz/ \
 {}/tags .*/v?(\d\S+)\.tar\.gz",
            source_package, url
        )?;
    } else if url.contains("gitlab.com") || url.contains("invent.kde.org") {
        writeln!(
            watch_file,
            r"{}/tags?sort=updated_desc .*/archive/v?(\d\S+)/.*\.tar\.gz.*",
            url
        )?;
    } else if url.contains("download.kde.org") {
        writeln!(
            watch_file,
            r"opts=pgpsigurlmangle=s/$/.sig/ \
 {}([\d.]+)/@PACKAGE@-v?(\d\S+)@ARCHIVE_EXT@ ([\d.]+)/@PACKAGE@@ANY_VERSION@@ARCHIVE_EXT@",
            url
        )?;
    } else {
        println!("Warning: Unable to generate automatic watch file. Please see https://wiki.debian.org/debian/watch on how to create one yourself");
    }

    Ok(())
}

fn main() {
    let mut control_file = ControlFile::new();

    /*
        Source paragraph
    */
    let mut source_paragraph = ControlParagraph::new();

    let maintainer = match env::var("NAME") {
        Ok(n) => match env::var("EMAIL") {
            Ok(e) => Maintainer { name: n, email: e },
            Err(_e) => ask_name_email(),
        },
        Err(_e) => ask_name_email(),
    };
    maintainer.export_to_env();

    ask_entry(
        &mut source_paragraph,
        "Source",
        "How is the project called upstream?",
        None,
    );
    ask_entry(
        &mut source_paragraph,
        "Section",
        "In which section does it fit? Examples are: comm, devel, games, net, sound, utils, misc",
        Some("misc"),
    );
    source_paragraph.add_entry("Priority", "optional".into());
    source_paragraph.add_entry("Maintainer", maintainer.clone().into());

    let deps = ask_input(
        "Which packages are needed to build this package? (comma seperated)",
        Some(""),
    );
    source_paragraph.add_entry(
        "Build-Depends",
        format!(
            "debhelper-compat (= {}), {}",
            DEBHELPER_COMPAT,
            match parse_dep_list(&deps) {
                Ok(_r) => deps,
                Err(_e) => {
                    println!("Warning: Dependency list is not parsable");
                    deps
                }
            }
        ),
    );

    source_paragraph.add_entry("Standards-Version", "4.5.0".to_string());

    let source_package = source_paragraph.get_entry("Source").unwrap().to_string();
    ask_entry(
        &mut source_paragraph,
        "Vcs-Git",
        "Where is your git repository going to be located?",
        Some(&format!("https://gitlab.com/debian-pm/{}", source_package)),
    );
    let git_url = source_paragraph.get_entry("Vcs-Git")
        .expect("Vcs-Git needs to be known at this time")
        .to_string();
    source_paragraph.add_entry(
        "Vcs-Browser",
        git_url.clone(),
    );

    control_file.add_paragraph(source_paragraph);

    /*
        Binary paragraph
    */
    let mut bin_paragraph = ControlParagraph::new();
    bin_paragraph.add_entry("Package", source_package.clone());
    bin_paragraph.add_entry("Architecture", "any".to_string());
    bin_paragraph.add_entry("Depends", "${shlibs:Depends}, ${misc:Depends}".to_string());

    let short_description = ask_input("Please describe the package in a few words", None);
    let long_description = ask_input("Please enter a long description for the package", None);
    bin_paragraph.add_entry(
        "Description",
        format!("{}\n {}", short_description, long_description),
    );

    control_file.add_paragraph(bin_paragraph);

    /*
        Write file
    */

    create_folders(&source_package);
    control_file
        .serialize(Path::new(&format!("{}/debian/control", source_package)))
        .expect("Failed to write control file");

    /*
        Copyright file (also control format)
    */

    let mut copyright_file = ControlFile::new();
    let mut copyright_header = ControlParagraph::new();
    copyright_header.add_entry(
        "Format",
        "https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/".to_string(),
    );
    copyright_header.add_entry("Upstream-Name", source_package.clone());
    copyright_file.add_paragraph(copyright_header);

    let mut debian_copyright = ControlParagraph::new();
    debian_copyright.add_entry("Files", "debian/*".to_string());
    let maintainer_str: String = maintainer.into();
    debian_copyright.add_entry(
        "Copyright",
        format!("{}, {}", chrono::Utc::now().year(), maintainer_str),
    );
    ask_entry(&mut debian_copyright, "License", "Which license do you want to use? (It should be more permissive than the license of the project)", Some("GPL-3.0+"));
    copyright_file.add_paragraph(debian_copyright);

    copyright_file
        .serialize(Path::new(&format!("{source_package}/debian/copyright")))
        .expect("Failed to write copyright");

    /*
        Copyright file
    */

    let mut rules_file = File::create(format!("{source_package}/debian/rules"))
        .expect("failed to create rules file");
    rules_file
        .write_all(
            b"#!/usr/bin/make -f
%:
\tdh $@
",
        )
        .expect("Failed to write rules");
    let extra_args = ask_input("Extra arguments to pass to the build system", Some(""));

    if !extra_args.is_empty() {
        rules_file
            .write_all(
                b"
override_dh_auto_configure:
\tdh_auto_configure -- ",
            )
            .expect("failed to append custom args");
        rules_file
            .write_all(extra_args.as_bytes())
            .expect("failed to append custom args");
    }

    /*
        Changelog file
    */

    let version = ask_input("Which version of the package is available?", Some("0.0"));

    let changelog_entry = ChangelogEntry::new(
        source_package.clone(),
        format!("{}-1", version),
        "  * Initial packaging\n".into(),
    );
    let changelog = Changelog::new(changelog_entry);
    changelog
        .to_file(Path::new(&format!("{source_package}/debian/changelog")))
        .expect("failed to write changelog");

    /*
        Source format
    */

    let mut compat_file = File::create(format!("{source_package}/debian/source/format"))
        .expect("failed to create format file");
    compat_file
        .write_all("3.0 (quilt)".as_bytes())
        .expect("Could not write debian/source/format");

    /*
        watch file
    */
    let url = ask_input("Where is the code download location of the project located? (Example: https://github.com/qxmpp-project/qxmpp", Some(""));
    generate_watch_file(&source_package, &url).expect("Failed to generate watch file");

    /*
        Initialize git repository
    */
    init_git_repository(&source_package);
    add_git_remote(&source_package, "origin", &git_url);
}
