// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-only

#[macro_use]
extern crate rocket;

use rocket::{
    data::ByteUnit,
    form::{self, error::ErrorKind, DataField, Errors, Form, FromForm, Options, ValueField},
    fs::TempFile,
    http::RawStr,
    tokio::fs::File,
    Build, State,
};

use std::{
    path::{Path, PathBuf},
    str,
    sync::Mutex,
};

use rand::{distributions::Alphanumeric, Rng}; // 0.8

mod reprepro;

const TMP_DIR: &str = "/tmp";
const META_FILE_SIZE_LIMIT: ByteUnit = ByteUnit::Megabyte(10);
const DATA_FILE_SIZE_LIMIT: ByteUnit = ByteUnit::Gigabyte(2);

fn unique_tmp_location() -> PathBuf {
    let rand_id: String = rand::thread_rng()
        .sample_iter(&Alphanumeric)
        .take(10)
        .map(char::from)
        .collect();

    let mut path = PathBuf::from(TMP_DIR);
    path.push(format!("reprepro-{}", rand_id));

    path
}

fn remove_temp_file(path: &Path) {
    if let Err(e) = std::fs::remove_file(path) {
        eprintln!("Failed to remove temporary file: {}", e);
    }
}

fn remove_temp_package_files(package: ChangesData) {
    for file in &package.attachments {
        remove_temp_file(file);
    }

    remove_temp_file(&package.metafile);

    package
        .metafile
        .parent()
        .and_then(|dir| std::fs::remove_dir(dir).ok());
}

#[derive(Responder)]
#[response(content_type = "text")]
enum CommandOutputResponse {
    #[response(status = 200)]
    Success(String),
    #[response(status = 415)]
    InvalidInput(String),
    #[response(status = 500)]
    InternalError(String),
}

fn send_command_result(res: reprepro::CommandResult) -> CommandOutputResponse {
    match res {
        Ok(stdout) => CommandOutputResponse::Success(stdout),
        Err(stderr) => CommandOutputResponse::InvalidInput(stderr),
    }
}

async fn save_stream<'r, 'i>(
    field: DataField<'r, 'i>,
    mut directory: PathBuf,
    size_limit: ByteUnit,
    ctxt: &mut FormParseContext<'r>,
) -> Option<PathBuf> {
    match field.file_name {
        Some(file_name) => {
            let raw_file_name = file_name.dangerous_unsafe_unsanitized_raw().as_str();
            match RawStr::new(raw_file_name).percent_decode() {
                Ok(decoded_name) => {
                    eprintln!("Saving stream, {:?}", decoded_name);

                    // check for bad file name
                    if decoded_name.contains('/') || decoded_name.contains("..") {
                        eprintln!("Illegal file name");
                        return None;
                    }

                    directory.push(decoded_name.as_ref());

                    if let Ok(file) = File::create(&directory).await {
                        let stream_result = field.data.open(size_limit).stream_to(file).await;

                        if let Err(e) = stream_result {
                            ctxt.errors.push(ErrorKind::Io(e).into());
                        }
                    }
                    Some(directory)
                }
                Err(e) => {
                    eprintln!(
                        "Received file name contained characters that could not be decoded: {e}"
                    );
                    None
                }
            }
        }
        None => {
            ctxt.errors.push(ErrorKind::Missing.into());
            println!("Data field received, but file name is missing");

            None
        }
    }
}

///
/// Contains the path to the changes file,
/// and to the attachments mentioned in it
///
struct ChangesData {
    metafile: PathBuf,
    attachments: Vec<PathBuf>,
}

struct FormParseContext<'c> {
    tempdir: PathBuf,
    metafile: Option<PathBuf>,
    attachments: Vec<PathBuf>,
    errors: Errors<'c>,
}

#[rocket::async_trait]
impl<'r> FromForm<'r> for ChangesData {
    type Context = FormParseContext<'r>;
    fn init(_: Options) -> Self::Context {
        let file_path = unique_tmp_location();
        std::fs::create_dir_all(&file_path).expect("The temporary directory is not writable");

        Self::Context {
            tempdir: file_path,
            metafile: None,
            attachments: Vec::new(),
            errors: Errors::new(),
        }
    }

    fn push_value(ctxt: &mut Self::Context, field: ValueField<'r>) {
        ctxt.errors.push(ErrorKind::Unexpected.into());
        eprintln!("Value field received, but expecting data field. The field {:?} is probably missing a Content-Type header", field.name.key());
    }

    async fn push_data(ctxt: &mut Self::Context, field: DataField<'r, '_>) {
        match field.name.to_string().as_str() {
            "changes" | "dsc" => {
                ctxt.metafile =
                    save_stream(field, ctxt.tempdir.clone(), META_FILE_SIZE_LIMIT, ctxt).await;
            }
            "attachments" => {
                if let Some(path) =
                    save_stream(field, ctxt.tempdir.clone(), DATA_FILE_SIZE_LIMIT, ctxt).await
                {
                    ctxt.attachments.push(path);
                }
            }
            _ => ctxt.errors.push(ErrorKind::Unexpected.into()),
        }
    }

    fn finalize(ctxt: Self::Context) -> form::Result<'r, Self> {
        match ctxt.metafile {
            Some(metafile) => Ok(ChangesData {
                metafile,
                attachments: ctxt.attachments,
            }),
            None => {
                let mut errors = Errors::new();
                errors.push(ErrorKind::Missing.into());
                Err(errors)
            }
        }
    }
}

#[post("/include/<distribution>", data = "<package>")]
fn include(
    distribution: String,
    package: Form<ChangesData>,
    repo: &State<Mutex<reprepro::Reprepro>>,
) -> CommandOutputResponse {
    let repoguard = repo.lock().unwrap();
    let result = repoguard.include(&distribution, &package.metafile);

    remove_temp_package_files(package.into_inner());

    send_command_result(result)
}

#[put("/includedeb/<distribution>", data = "<package>")]
async fn includedeb(
    distribution: String,
    mut package: TempFile<'_>,
    repo: &State<Mutex<reprepro::Reprepro>>,
) -> CommandOutputResponse {
    let tempfile = unique_tmp_location();

    if let Err(e) = package.persist_to(&tempfile).await {
        eprintln!("Failed to store temporary file!: {}", e);
        return CommandOutputResponse::InternalError(
            "Failed to store temporary file, see server log for details".to_owned(),
        );
    }

    let repoguard = repo.lock().unwrap();

    let result = repoguard.includedeb(&distribution, &[&tempfile]);

    remove_temp_file(&tempfile);

    send_command_result(result)
}

#[post("/includedsc/<distribution>", data = "<package>")]
fn includedsc(
    distribution: String,
    package: Form<ChangesData>,
    repo: &State<Mutex<reprepro::Reprepro>>,
) -> CommandOutputResponse {
    let repoguard = repo.lock().unwrap();
    let result = repoguard.includedsc(&distribution, &package.metafile);

    remove_temp_package_files(package.into_inner());

    send_command_result(result)
}

#[post("/export")]
fn export(repo: &State<Mutex<reprepro::Reprepro>>) -> CommandOutputResponse {
    let repoguard = repo.lock().unwrap();
    send_command_result(repoguard.export())
}

#[launch]
fn rocket() -> rocket::Rocket<Build> {
    rocket::build()
        .mount("/", routes![includedeb, includedsc, include, export])
        .manage(Mutex::from(reprepro::Reprepro::new(
            std::env::current_dir().expect("Failed to find current directory"),
        )))
}
