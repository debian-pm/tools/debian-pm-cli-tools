// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-only

use std::path::Path;
use std::process::Command;
use std::process::Output;
use std::result::Result;
use std::{io::Result as IOResult, path::PathBuf};

pub type CommandResult = Result<String, String>;

pub fn io_result_to_command_result(result: IOResult<Output>) -> CommandResult {
    match result {
        Ok(o) => {
            if o.status.success() {
                CommandResult::Ok(String::from_utf8_lossy(&o.stdout).to_string())
            } else {
                CommandResult::Err(String::from_utf8_lossy(&o.stderr).to_string())
            }
        }
        Err(e) => CommandResult::Err(format!("{}", e)),
    }
}

pub fn run_command(command: &mut Command) -> CommandResult {
    io_result_to_command_result(command.output())
}

pub struct Reprepro {
    path: PathBuf,
}

impl Reprepro {
    pub fn new(path: PathBuf) -> Reprepro {
        Reprepro { path }
    }

    pub fn include(&self, distro: &str, changes_file: &Path) -> CommandResult {
        run_command(
            Command::new("reprepro")
                .arg("-b")
                .arg(&self.path)
                .arg("--ignore=wrongdistribution")
                .arg("--ignore=extension")
                .arg("--keepunreferencedfiles")
                .arg("include")
                .arg(distro)
                .arg(changes_file),
        )
    }

    pub fn includedeb(&self, distro: &str, deb_files: &[&Path]) -> CommandResult {
        run_command(
            Command::new("reprepro")
                .arg("-b")
                .arg(&self.path)
                .arg("--ignore=extension")
                .arg("--export=never")
                .arg("--keepunreferencedfiles")
                .arg("includedeb")
                .arg(distro)
                .args(deb_files),
        )
    }

    pub fn export(&self) -> CommandResult {
        run_command(
            Command::new("reprepro")
                .arg("-b")
                .arg(&self.path)
                .arg("export")
        )
    }

    pub fn includedsc(&self, distro: &str, dsc_file: &Path) -> CommandResult {
        run_command(
            Command::new("reprepro")
                .arg("-b")
                .arg(&self.path)
                .arg("--ignore=extension")
                .arg("--keepunreferencedfiles")
                .arg("includedsc")
                .arg(distro)
                .arg(dsc_file),
        )
    }
}
