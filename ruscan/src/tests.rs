// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-only

use crate::{Mode, PGPMode, SearchMode, WatchFile};

#[cfg(test)]
use regex::Regex;

#[test]
fn parse_bitbucket() {
    let result = WatchFile::from_string(
        r"version=4
https://bitbucket.org/<user>/<project>/downloads?tab=tags .*/(\d\S+)\.tar\.gz",
    );

    assert!(result.is_ok());
    let watch_files = result.unwrap();

    assert!(watch_files[0].url.is_some());
    assert_eq!(
        watch_files[0].url.as_ref().unwrap(),
        "https://bitbucket.org/<user>/<project>/downloads?tab=tags"
    );
    assert!(watch_files[0].pattern.is_some());
    assert_eq!(
        watch_files[0].pattern.as_ref().unwrap(),
        r".*/(\d\S+)\.tar\.gz"
    );
    assert!(watch_files[0].opts.is_none());

    assert!(Regex::new(watch_files[0].pattern.as_ref().unwrap()).is_ok())
}

#[test]
fn parse_github() {
    let result = WatchFile::from_string(
        r"version=4
opts=filenamemangle=s/.+\/v?(\d\S+)\.tar\.gz/<project>-$1\.tar\.gz/ \
https://github.com/<user>/<project>/tags .*/v?(\d\S+)\.tar\.gz",
    );

    assert!(result.is_ok());
    let watch_files = result.unwrap();

    assert!(watch_files[0].url.is_some());
    assert_eq!(
        watch_files[0].url.as_ref().unwrap(),
        r"https://github.com/<user>/<project>/tags"
    );

    assert!(watch_files[0].pattern.is_some());
    assert_eq!(
        watch_files[0].pattern.as_ref().unwrap(),
        r".*/v?(\d\S+)\.tar\.gz"
    );
    assert!(Regex::new(watch_files[0].pattern.as_ref().unwrap()).is_ok());

    assert!(watch_files[0].opts.is_some());
    assert_eq!(
        watch_files[0].opts.as_ref().unwrap().filenamemangle,
        r"s/.+/v?(dS+).tar.gz/<project>-$1.tar.gz/"
    );
}

#[test]
fn parse_gitlab() {
    let result = WatchFile::from_string(
        r"version=4
    opts=filenamemangle=s/.*\/archive\/(\d\S+)\/<project>.*\.tar\.gz/<project>-$1\.tar\.gz/g \
      https://gitlab.com/<user>/<project>/tags?sort=updated_desc .*/archive/(\d\S+)/.*\.tar\.gz.*

    opts=filenamemangle=s/.*\/archive\/(\d\S+)\/<project>.*\.tar\.gz/<project>-$1\.tar\.gz/g \
      https://gitlab.com/<user>/<project>/tags?sort=updated_desc .*/archive/(\d\S+)/.*\.tar\.gz.*",
    );

    assert!(result.is_ok());
    let watch_files = result.unwrap();

    assert!(watch_files[0].url.is_some());
    assert_eq!(
        watch_files[0].url.as_ref().unwrap(),
        r"https://gitlab.com/<user>/<project>/tags?sort=updated_desc"
    );

    assert!(watch_files[0].pattern.is_some());
    assert_eq!(
        watch_files[0].pattern.as_ref().unwrap(),
        r".*/archive/(\d\S+)/.*\.tar\.gz.*"
    );
    assert!(Regex::new(watch_files[0].pattern.as_ref().unwrap()).is_ok());

    assert!(watch_files[0].opts.is_some());
    assert_eq!(
        watch_files[0].opts.as_ref().unwrap().filenamemangle,
        r"s/.*/archive/(dS+)/<project>.*.tar.gz/<project>-$1.tar.gz/g"
    );

    assert!(watch_files[1].url.is_some());
}

#[test]
fn parse_launchpad() {
    let result = WatchFile::from_string(
        r"version=4
    opts=pgpsigurlmangle=s/$/.asc/ \
      https://launchpad.net/<project>/ \
      https://launchpad.net/.*download/<project>-([.\d]+)\.tar\.xz",
    );

    assert!(result.is_ok());
    let watch_files = result.unwrap();

    assert!(watch_files[0].url.is_some());
    assert_eq!(
        watch_files[0].url.as_ref().unwrap(),
        r"https://launchpad.net/<project>/"
    );

    assert!(watch_files[0].pattern.is_some());
    assert_eq!(
        watch_files[0].pattern.as_ref().unwrap(),
        r"https://launchpad.net/.*download/<project>-([.\d]+)\.tar\.xz"
    );
    assert!(Regex::new(watch_files[0].pattern.as_ref().unwrap()).is_ok());

    assert!(watch_files[0].opts.is_some());
    assert_eq!(
        watch_files[0].opts.as_ref().unwrap().pgpsigurlmangle,
        r"s/$/.asc/"
    );
}

#[test]
fn parse_pgpmode() {
    let result = WatchFile::from_string(
        r#"version=4
    opts=pgpmode=auto"#,
    );

    assert!(result.is_ok());
    let watch_files = result.unwrap();

    assert!(watch_files[0].opts.is_some());
    assert!(matches!(
        watch_files[0].opts.as_ref().unwrap().pgpmode,
        Some(PGPMode::Auto)
    ));
}

#[test]
fn parse_search_mode() {
    let result = WatchFile::from_string(
        r#"version=4
    opts=searchmode=html"#,
    );

    assert!(result.is_ok());
    let watch_files = result.unwrap();

    assert!(watch_files[0].opts.is_some());
    assert!(matches!(
        watch_files[0].opts.as_ref().unwrap().searchmode,
        Some(SearchMode::Html)
    ));
}

#[test]
fn parse_mode() {
    let result = WatchFile::from_string(
        r#"version=4
    opts=mode=git"#,
    );

    assert!(result.is_ok());
    let watch_files = result.unwrap();

    assert!(watch_files[0].opts.is_some());
    assert!(matches!(
        watch_files[0].opts.as_ref().unwrap().mode,
        Some(Mode::Git)
    ))
}

#[test]
fn from_file() {
    let watch_files = WatchFile::from_file("test_files/watch");
    assert!(watch_files.is_ok());
    assert!(watch_files.unwrap()[0].url.is_some());
}

#[test]
fn expand() {
    let result = WatchFile::from_string(
        r#"version=4
    http://lol.xd/download @ARCHIVE_EXT@"#,
    );

    assert_eq!(
        result.unwrap().first().unwrap().pattern.as_ref().unwrap(),
        r"(?i)\.(?:tar\.xz|tar\.bz2|tar\.gz|zip|tgz|tbz|txz)"
    )
}
