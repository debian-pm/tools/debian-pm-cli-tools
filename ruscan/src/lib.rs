// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-only

use std::fmt::Debug;
use std::fs::File;
use std::io::prelude::*;

#[cfg(test)]
mod tests;

#[derive(Default, Debug, Clone)]
pub struct Opts {
    // TODO:
    // Parse options into structs, not strings
    pub component: String,
    pub compression: String,
    pub repack: String,
    pub repacksuffix: String,
    pub mode: Option<Mode>,
    pub pretty: String,
    pub date: String,
    pub gitmode: String,
    pub pgpmode: Option<PGPMode>,
    pub searchmode: Option<SearchMode>,
    pub decompress: String,
    pub bare: String,
    pub user_agent: String,
    pub passive: String,
    pub active: String,
    pub unzipopt: String,
    pub dversionmangle: String,
    pub dirversionmangle: String,
    pub pagemangle: String,
    pub uversionmangle: String,
    pub versionmangle: String,
    pub hrefdecode: String,
    pub downloadurlmangle: String,
    pub filenamemangle: String,
    pub pgpsigurlmangle: String,
    pub oversionmangle: String,
}

#[derive(Debug, Clone)]
pub enum Mode {
    LWP,
    Git,
}

impl Mode {
    fn parse_from_string(mode_s: &str) -> Option<Self> {
        match mode_s {
            "LWP" => Some(Self::LWP),
            "git" => Some(Self::Git),
            _ => None,
        }
    }
}

#[derive(Debug, Clone)]
pub enum PGPMode {
    Auto,
    Default,
    Mangle,
    Next,
    Previous,
    SelfSig,
    Gitag,
    None,
}

impl PGPMode {
    fn parse_from_string(pgp_mode_s: &str) -> Option<Self> {
        match pgp_mode_s {
            "auto" => Some(Self::Auto),
            "default" => Some(Self::Default),
            "mangle" => Some(Self::Mangle),
            "next" => Some(Self::Next),
            "previous" => Some(Self::Previous),
            "self" => Some(Self::SelfSig),
            "gittag" => Some(Self::Gitag),
            "none" => Some(Self::None),
            _ => None,
        }
    }
}

#[derive(Debug, Clone)]
pub enum SearchMode {
    Html,
    Plain,
}

impl SearchMode {
    fn parse_from_string(search_mode_s: &str) -> Option<Self> {
        match search_mode_s {
            "html" => Some(Self::Html),
            "plain" => Some(Self::Plain),
            _ => None,
        }
    }
}

impl Opts {
    /// Parse options from options={} string into struct
    fn parse_from_string(options_s: &str) -> Opts {
        let mut options = Opts::default();

        for opt in options_s.split(',') {
            let mut split = opt.split('=');
            let (key, value) = (split.next().unwrap(), split.next().unwrap());

            match key {
                "component" => options.component = value.to_owned(),
                "compression" => options.compression = value.to_owned(),
                "repack" => options.repack = value.to_owned(),
                "repacksuffix" => options.repacksuffix = value.to_owned(),
                "mode" => options.mode = Mode::parse_from_string(value),
                "pretty" => options.pretty = value.to_owned(),
                "date" => options.date = value.to_owned(),
                "gitmode" => options.gitmode = value.to_owned(),
                "pgpmode" => options.pgpmode = PGPMode::parse_from_string(value),
                "searchmode" => options.searchmode = SearchMode::parse_from_string(value),
                "decompress" => options.decompress = value.to_owned(),
                "bare" => options.decompress = value.to_owned(),
                "user-agent" => options.user_agent = value.to_owned(),
                "pasv" | "passive" => options.passive = value.to_owned(),
                "unzipopt" => options.unzipopt = value.to_owned(),
                "dversionmangle" => options.dversionmangle = value.to_owned(),
                "dirversionmangle" => options.dirversionmangle = value.to_owned(),
                "pagemangle" => options.pagemangle = value.to_owned(),
                "uversionmangle" => options.uversionmangle = value.to_owned(),
                "versionmangle" => options.versionmangle = value.to_owned(),
                "hrefdecode" => options.hrefdecode = value.to_owned(),
                "downloadurlmangle" => options.downloadurlmangle = value.to_owned(),
                "filenamemangle" => options.filenamemangle = value.to_owned(),
                "pgpsigurlmangle" => options.pgpsigurlmangle = value.to_owned(),
                "oversionmangle" => options.oversionmangle = value.to_owned(),
                _ => {}
            }
        }

        options
    }
}

#[derive(Default, Debug, Clone)]
pub struct WatchFile {
    pub opts: Option<Opts>,
    pub url: Option<String>,
    pub pattern: Option<String>,
}

impl WatchFile {
    pub fn from_string(file_contents: &str) -> Result<Vec<WatchFile>, &'static str> {
        WatchFile::parse(WatchFile::expand(&WatchFile::prepare(file_contents)?))
    }

    pub fn from_file(file_path: &str) -> Result<Vec<WatchFile>, &str> {
        let mut watch = String::new();

        match File::open(file_path) {
            Ok(mut o) => match o.read_to_string(&mut watch) {
                Ok(_r) => Ok(WatchFile::from_string(&watch)?),
                Err(_e) => Err("Failed to read file"),
            },
            Err(_e) => Err("Failed to open file"),
        }
    }

    /// begin of parsing. According to the manpage, we need to do the following:
    /// The current version 4 format of debian/watch can be summarized as follows:
    /// •   Leading spaces and tabs are dropped.
    /// •   Empty lines are dropped.
    /// •   A line started by # (hash) is a comment line and dropped.
    /// •   A single \ (back slash) at the end of a line is dropped and the next line is concatenated after removing leading spaces and tabs. The
    ///     concatenated line is parsed as a single line. (The existence or non-
    ///     existence of the space before the tailing single \ is significant.)
    ///     line is concatenated after removing leading spaces and tabs. The
    ///     concatenated line is parsed as a single line. (The existence or non-
    ///     existence of the space before the tailing single \ is significant.)
    fn prepare(watch: &str) -> Result<String, &'static str> {
        let mut watch_content = watch.to_owned();
        let mut watch_lines = String::new();

        if !watch_content.starts_with("version=4") {
            return Err("Only the version 4 format is supported, help: If there is no version set, consider adding version=4 to the beginning of the watch file");
        }
        watch_content = watch_content.replace("version=4", "");

        for mut line in watch_content.lines() {
            // drop whitespaces
            line = line.trim();

            // Drop empty and comment lines
            if line.is_empty() || line.starts_with('#') {
                continue;
            }

            if line.ends_with(r" \") {
                // Remove \ and remove \n
                watch_lines.push_str(&line.replace('\\', ""));
            } else {
                watch_lines.push_str(&format!("{}\n", line));
            }
        }

        Ok(watch_lines)
    }

    fn expand(text: &str) -> String {
        text.replace("@ANY_VERSION@", r"[-_]?(\d[\-+\.:\~\da-zA-Z]*)")
            .replace(
                "@ARCHIVE_EXT@",
                r"(?i)\.(?:tar\.xz|tar\.bz2|tar\.gz|zip|tgz|tbz|txz)",
            )
            .replace(
                "@SIGNATURE_EXT@",
                r"(?i)\.(?:tar\.xz|tar\.bz2|tar\.gz|zip|tgz|tbz|txz)\.(?:asc|pgp|gpg|sig|sign)",
            )
            .replace("@DEB_EXT@", r"[\+~](debian|dfsg|ds|deb)(\.)?(\d+)?$")
    }

    /// This function takes a already prepared watch line,
    /// meaning that it is not allowed to contain version=n tag or
    /// unconcentrated lines ending with \
    fn parse(watch_lines: String) -> Result<Vec<WatchFile>, &'static str> {
        let mut watch_files: Vec<WatchFile> = Vec::new();

        for line in watch_lines.lines() {
            /*
                split up string
            */
            let mut opts_s = String::new();
            let mut url_s = String::new();
            let mut pattern_s = String::new();

            // url is the first after opts, pattern the second
            // therefore we need to count
            let mut part_n: i8 = 0;
            for part in line.split(' ') {
                if part.starts_with("opts=") {
                    opts_s = part.replace("opts=", "");
                } else if (part.starts_with("https://") || part.starts_with("http://"))
                    && part_n == 0
                {
                    part_n += 1;
                    url_s = part.to_owned();
                } else if part_n == 1 {
                    part_n += 1;
                    pattern_s = part.to_owned();
                }
            }

            /*
                parse options into sturct
            */
            let options = if !opts_s.is_empty() {
                Some(Opts::parse_from_string(&opts_s))
            } else {
                None
            };

            let watch_file = WatchFile {
                opts: options,
                url: if url_s.is_empty() { None } else { Some(url_s) },
                pattern: if pattern_s.is_empty() {
                    None
                } else {
                    Some(pattern_s)
                },
            };

            watch_files.push(watch_file);
        }

        Ok(watch_files)
    }
}
