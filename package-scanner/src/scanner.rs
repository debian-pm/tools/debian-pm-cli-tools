// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-only

use std::collections::BTreeMap;
use std::fs::metadata;
use std::path::Path;
use std::sync::{Arc, Mutex};

use debarchive::Archive;
use debian::package::*;
use walkdir::WalkDir;

use threadpool::ThreadPool;

use crate::hashing::{hash_file, Algorithm};

#[derive(Clone)]
struct PackageMeta {
    map: Option<BTreeMap<String, String>>,
    filename: String,
    md5sum: String,
    sha1: String,
    sha256: String,
}

fn read_package(package_filename: &str) -> Option<BTreeMap<String, String>> {
    let path = &Path::new(&package_filename);
    if !path.exists() {
        println!(
            "Warning: File {} does not exist, skipping!",
            package_filename
        );
        return None;
    };
    let archive = Archive::new(path).unwrap();

    Some(archive.control_map().unwrap())
}

///
/// Scan the basepath for deb packages, and write the result into $basepath/Packages
///
pub fn scan(basepath: &str, verbose: bool) {
    let mut files: Vec<String> = Vec::new();

    for entry in WalkDir::new(basepath)
        .follow_links(true)
        .into_iter()
        .filter_entry(|e| {
            e.file_name()
                .to_str()
                .map(|s| s != "debian" && s != ".git")
                .unwrap_or(true)
        })
        .filter_map(|e| e.ok())
    {
        if let Some(file_name) = entry.file_name().to_str() {
            if file_name.ends_with(".deb") {
                files.push(entry.path().to_str().unwrap().to_owned());
            }
        }
    }

    let mut packages_control = if Path::new(&format!("{}/Packages", &basepath)).is_file() {
        // Read existing file, for incremental updating
        ControlFile::from_file(Path::new(&format!("{}/Packages", &basepath)))
            .expect("Failed to read existing Packages file")
    } else {
        ControlFile::new()
    };

    let pool = ThreadPool::new(num_cpus::get());
    let results = Arc::from(Mutex::from(Vec::<PackageMeta>::new()));
    for package_filename in files {
        let results_handle = results.clone();
        pool.execute(move || {
            let meta = PackageMeta {
                map: read_package(&package_filename),
                md5sum: hash_file(&package_filename, Algorithm::MD5)
                    .unwrap()
                    .to_lowercase(),
                sha1: hash_file(&package_filename, Algorithm::SHA1)
                    .unwrap()
                    .to_lowercase(),
                sha256: hash_file(&package_filename, Algorithm::SHA2256)
                    .unwrap()
                    .to_lowercase(),
                filename: package_filename,
            };
            results_handle.lock().unwrap().push(meta);
        });
    }

    pool.join();
    let results = (*results.lock().unwrap()).clone();
    for meta in results {
        if let Some(control_map) = meta.map {
            let package_filename = meta.filename;
            let path = &Path::new(&package_filename);
            let mut paragraph = ControlParagraph::new();

            // Copy over entries from the package control section
            for key in control_map.keys() {
                paragraph.add_entry(key, control_map.get(key).unwrap().to_owned());
            }

            // Add repository specific entries
            paragraph.add_entry("Filename", package_filename.replace(basepath, ""));
            paragraph.add_entry("Size", metadata(path).unwrap().len().to_string());
            paragraph.add_entry("MD5sum", meta.md5sum);
            paragraph.add_entry("SHA1", meta.sha1);
            paragraph.add_entry("SHA256", meta.sha256);

            packages_control.add_paragraph(paragraph);
            if verbose {
                println!("Added {}", package_filename);
            }
        }
    }
    packages_control
        .serialize(Path::new(&format!("{}/Packages", &basepath)))
        .expect("Could not write Packages");
}
