// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-only

extern crate debarchive;
extern crate debian;
extern crate walkdir;

use clap::{Arg, Command, ArgAction};

mod hashing;
mod scanner;

use crate::scanner::scan;

fn main() {
    let app = Command::new("Package Scanner")
        .version("0.1")
        .author("Jonah Brüchert <jbb@kaidan.im>")
        .arg(
            Arg::new("basepath")
                .long("basepath")
                .short('b')
                .value_name("path")
                .help("Path that will be scanned for packages")
                .action(ArgAction::Set)
                .required(true),
        )
        .arg(
            Arg::new("verbose")
                .long("verbose")
                .short('v')
                .value_name("bool")
                .help("Log verbose information")
                .required(false)
                .action(ArgAction::SetTrue)
        );

    let matches = app.get_matches();

    scan(
        matches.get_one::<String>("basepath").unwrap(),
        matches.get_flag("verbose"),
    );
}
