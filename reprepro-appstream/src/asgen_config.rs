// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-only

use debian::package::{ControlFile, ControlParagraph};

use std::{collections::HashMap, path::Path, str};

#[derive(Serialize)]
pub struct Suite {
    pub architectures: Vec<String>,
    pub sections: Vec<String>,
}

#[derive(Serialize)]
#[serde(rename_all = "PascalCase")]
pub struct ExportDirs {
    pub media: String,
    pub data: String,
    pub hints: String,
    pub html: String,
}

#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
pub struct Features {
    metadata_timestamps: bool,
}

#[derive(Serialize)]
#[serde(rename_all = "PascalCase")]
pub struct AsgenConfig {
    pub archive_root: String,
    pub backend: String,
    pub html_base_url: String,
    pub media_base_url: String,
    pub project_name: String,
    pub suites: HashMap<String, Suite>,
    pub export_dirs: ExportDirs,
    pub features: Features,
}

fn control_get_key(control: &ControlParagraph, key: &str) -> String {
    control
        .get_entry(key)
        .unwrap_or_else(|| panic!("Required key {} is missing", key))
        .to_owned()
}

fn control_get_list_key(control: &ControlParagraph, key: &str) -> Vec<String> {
    control
        .get_entry(key)
        .unwrap_or_else(|| panic!("Required key {} is missing", key))
        .split_ascii_whitespace()
        .map(|s| s.to_owned())
        .collect()
}

impl AsgenConfig {
    pub fn generate(reprepro_conf_dir: &Path, archive_root: &str) -> AsgenConfig {
        let mut conf_dist = reprepro_conf_dir.to_path_buf();
        conf_dist.push("distributions");

        let mut suites: HashMap<String, Suite> = HashMap::new();
        let mut origin: Option<String> = None;

        match ControlFile::from_file(&conf_dist) {
            Ok(distributions) => {
                for dist in distributions.get_paragraphs() {
                    let suite = Suite {
                        architectures: control_get_list_key(dist, "Architectures")
                            .into_iter()
                            .filter(|a| a != "source")
                            .collect(),
                        sections: control_get_list_key(dist, "Components"),
                    };

                    suites.insert(
                        dist.get_entry("Codename")
                            .expect("Required key Codename not found")
                            .to_owned(),
                        suite,
                    );

                    origin = dist.get_entry("Origin").map(|s| s.to_owned())
                }
            }
            Err(e) => {
                eprintln!(
                    "The reprepro conf/distributions file is not parsable: {}",
                    e
                );
                std::process::exit(1);
            }
        }

        let mut conf_appstream = reprepro_conf_dir.to_path_buf();
        conf_appstream.push("appstream");

        let base_url = match ControlFile::from_file(&conf_appstream) {
            Ok(appstream) => match appstream.get_paragraphs().first() {
                Some(p) => control_get_key(p, "BaseUrl"),
                None => {
                    println!("conf/appstream needs to have exactly one paragraph");
                    std::process::exit(1)
                }
            },
            Err(e) => {
                eprintln!("The reprepro conf/appstream file is not parsable: {}", e);
                std::process::exit(1);
            }
        };

        let appstream_base_dir = archive_root.to_string() + "/appstream/";
        let appstream_base_url = base_url + "/appstream/";

        AsgenConfig {
            archive_root: archive_root.to_string(),
            backend: "debian".to_owned(),
            media_base_url: appstream_base_url.to_string() + "/media/",
            html_base_url: appstream_base_url + "/html/",
            project_name: origin.unwrap_or_else(|| "Unknown".to_owned()),
            suites,
            export_dirs: ExportDirs {
                media: appstream_base_dir.to_string() + "/media/",
                hints: appstream_base_dir.to_string() + "/hints",
                html: appstream_base_dir.to_string() + "/html/",
                data: appstream_base_dir + "/data/",
            },
            features: Features {
                metadata_timestamps: false,
            },
        }
    }
}
