// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-only

use lazy_static::lazy_static;

lazy_static! {
    ///
    /// Set DPM_DOCKER_VERBOSE to something to enable verbose docker logging.
    ///
    pub static ref DOCKER_VERBOSE: bool = std::env::var("DPM_DOCKER_VERBOSE").is_ok();
}

#[macro_export]
macro_rules! println_colour {
    ($colour:expr, $($arg:tt)*) => {
        println!("{}", $colour.paint($($arg)*));
    };
}

#[macro_export]
macro_rules! println_status {
    ($arg:literal) => {
        use $crate::println_colour;
        use ansi_term::Colour;

        println_colour!(Colour::Blue, format!("-- {}", $arg));
    };
    ($($arg:tt)*) => {
        use $crate::println_colour;
        use ansi_term::Colour;

        println_colour!(Colour::Blue, format!("-- {}", format!($($arg)*)));
    }
}

#[macro_export]
macro_rules! println_command {
    ($arg:literal) => {
        use $crate::println_colour;
        use ansi_term::Colour;

        println_colour!(Colour::Green, format!("$ {}", $arg));
    };
    ($($arg:tt)*) => {
        use $crate::println_colour;
        use ansi_term::Colour;

        println_colour!(Colour::Green, format!("$ {}", $($arg)*));
    }
}

#[macro_export]
macro_rules! docker_log {
    ($($arg:tt)*) => {
        use $crate::log;
        if *log::DOCKER_VERBOSE {
            println!($($arg)*);
        }
    }
}
