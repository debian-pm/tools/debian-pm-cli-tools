// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-only

pub use std::io::Result;
use std::process::{Command, Output, Stdio};

use serde::*;

use crate::docker_log;
use crate::println_command;
use crate::println_status;

///
/// Result type that contains the stderr of a command on error, and nothing on success
///
pub type CommandResult = std::result::Result<(), String>;

///
/// Check the error code of a command and converts it into a container::CommandResult
///
pub fn check(res: Result<Output>) -> CommandResult {
    if let Ok(result) = res {
        if !result.status.success() {
            Err(String::from_utf8(result.stderr).unwrap_or_default())
        } else {
            Ok(())
        }
    } else {
        Err(String::new())
    }
}

///
/// Check the error code of a command and handle it.
/// It is suitable for functions that can't return CommandResult, but std::io::Result
///
#[macro_export]
macro_rules! io_check {
    ($res:expr) => {
        let res = $res;
        if let Ok(result) = res {
            if !result.status.success() {
                return Err(std::io::Error::new(
                    std::io::ErrorKind::Other,
                    String::from_utf8(result.stderr).unwrap_or_default(),
                ));
            }
        } else {
            return Err(res.unwrap_err());
        }
    };
}

///
/// Basic docker cli commands
///
pub struct Docker {}

impl Docker {
    fn cli(args: &[&str], capture: bool) -> Result<Output> {
        docker_log!("Executing docker {:?}", args);
        let mut command = Command::new("sudo");
        command.args([&["docker"], args].concat());

        if !capture {
            command.stdout(Stdio::inherit()).stderr(Stdio::inherit());
        }

        command.output()
    }

    pub fn cli_capture(args: &[&str]) -> Result<Output> {
        Docker::cli(args, true)
    }

    pub fn cli_output(args: &[&str]) -> Result<Output> {
        Docker::cli(args, false)
    }

    fn inspect_container(id: &str) -> Result<DockerInspect> {
        docker_log!("Inspecting container with url {}", id);
        let inspect = String::from_utf8(Docker::cli_capture(&["inspect", id])?.stdout).unwrap();
        let mut inspect = serde_json::from_str::<Vec<DockerInspect>>(&inspect)?;

        if inspect.is_empty() {
            docker_log!("Could not inspect container, most likely it does not exist");
            return Err(std::io::Error::from(std::io::ErrorKind::NotFound));
        }

        // We assume inspect to only return one element, since we are only passing one id
        assert_eq!(inspect.len(), 1);
        Ok(inspect.pop().unwrap())
    }
}

///
/// Structure representing a mount point
///
pub struct Mount {
    from: String,
    to: String,
}

impl Mount {
    pub fn to_args(&self) -> Vec<String> {
        vec!["-v".to_owned(), format!("{}:{}", &self.from, &self.to)]
    }

    pub fn new(from: &str, to: &str) -> Mount {
        Mount {
            from: from.to_owned(),
            to: to.to_owned(),
        }
    }
}

///
/// Options that can be passed to docker run
///
#[derive(Default)]
pub struct RunOptions {
    mounts: Vec<Mount>,
    delete_on_drop: Option<bool>,
}

impl RunOptions {
    pub fn to_args(&self) -> Vec<String> {
        let mut args = Vec::<String>::new();
        for mount in &self.mounts {
            args.append(&mut mount.to_args());
        }
        args
    }

    pub fn mount(mut self, from: &str, to: &str) -> Self {
        self.mounts.push(Mount::new(from, to));
        self
    }

    pub fn delete_on_drop(mut self, delete: bool) -> Self {
        self.delete_on_drop = Some(delete);
        self
    }
}

#[test]
fn test_run_options() {
    let args = RunOptions::default()
        .mount("/path/to/packages", "/packages")
        .mount("/home", "/home")
        .to_args();
    assert_eq!(
        args,
        vec!["-v", "/path/to/packages:/packages", "-v", "/home:/home"]
    );
}

struct EnvironmentVariable {
    key: String,
    value: String,
}

impl EnvironmentVariable {
    fn new(key: &str, value: &str) -> EnvironmentVariable {
        EnvironmentVariable {
            key: key.to_string(),
            value: value.to_string(),
        }
    }

    fn to_args(&self) -> Vec<String> {
        vec!["--env".to_owned(), format!("{}={}", self.key, self.value)]
    }
}

#[test]
fn test_env_var() {
    assert_eq!(
        EnvironmentVariable::new("FOO", "bar").to_args(),
        vec!["--env", "FOO=bar"]
    );
}

///
/// Options that can be passed to docker exec
///
#[derive(Default)]
pub struct ExecOptions {
    workdir: Option<String>,
    capture: Option<bool>,
    env_vars: Vec<EnvironmentVariable>,
    user: Option<String>,
}

impl ExecOptions {
    fn to_args(&self) -> Vec<String> {
        let mut args = Vec::<String>::new();
        if let Some(workdir) = &self.workdir {
            args.push("--workdir".to_owned());
            args.push(workdir.clone());
        }
        if let Some(user) = &self.user {
            args.push("--user".to_owned());
            args.push(user.clone());
        }
        for var in &self.env_vars {
            args.append(&mut var.to_args());
        }
        args
    }

    pub fn workdir(mut self, workdir: &str) -> Self {
        self.workdir = Some(workdir.to_owned());
        self
    }

    pub fn env(mut self, key: &str, value: &str) -> Self {
        self.env_vars.push(EnvironmentVariable::new(key, value));
        self
    }

    pub fn capture(mut self, capture: bool) -> Self {
        self.capture = Some(capture);
        self
    }

    pub fn user(mut self, user: &str) -> Self {
        self.user = Some(user.to_owned());
        self
    }
}

#[test]
fn test_exec_options() {
    let args = ExecOptions::default()
        .env("FOO", "bar")
        .env("MESSAGE", "Hello World")
        .user("konqi")
        .workdir("/tmp")
        .to_args();

    assert_eq!(
        args,
        vec![
            "--workdir",
            "/tmp",
            "--user",
            "konqi",
            "--env",
            "FOO=bar",
            "--env",
            "MESSAGE=Hello World"
        ]
    );
}

#[derive(Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct DockerInspectConfig {
    pub image: String,
    pub hostname: String,
}

#[derive(Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct DockerInspectState {
    pub status: String,
    pub running: bool,
    pub paused: bool,
}

#[derive(Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct DockerInspectNetworkSettings {
    #[serde(rename = "IPAddress")]
    pub ip_address: String,
}

///
/// Data from docker inspect
///
#[derive(Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct DockerInspect {
    pub id: String,
    pub config: DockerInspectConfig,
    pub network_settings: DockerInspectNetworkSettings,
    pub state: DockerInspectState,
    // Add more when needed
}

///
/// Docker container
///
pub struct Container {
    id: String,
    delete_on_drop: bool,
    inspect: DockerInspect,
}

impl Container {
    pub fn start_with_options(image: &str, options: RunOptions) -> Result<Container> {
        docker_log!("Starting new container");
        Docker::cli_output(&["pull", image])?;
        let mut arguments = vec!["run", "-it", "-d"];
        let opt_args = options.to_args();
        arguments.append(&mut opt_args.iter().map(|o| o.as_str()).collect());
        arguments.push(image);
        arguments.push("/bin/sh");

        let id = Docker::cli_capture(&arguments)?.stdout;
        assert!(!id.is_empty(), "Returned id of new container was empty");

        let id = String::from_utf8(id).unwrap().trim().to_string(); // Docker doesn't print invalid utf8

        let inspect = Docker::inspect_container(&id)?;

        assert!(inspect.state.running, "Container stopped unexpectedly");
        assert_eq!(inspect.state.status, "running");
        assert_eq!(inspect.config.image, image);

        Ok(Container {
            inspect,
            id,
            delete_on_drop: options.delete_on_drop.unwrap_or(false),
        })
    }

    ///
    /// Picks up an existing container, starts it and inspects its properties
    ///
    pub fn from_existing_id(id: &str, delete_on_drop: bool) -> Result<Container> {
        Docker::cli_capture(&["start", id])?;
        docker_log!("Using existing container");
        let inspect = Docker::inspect_container(id)?;

        assert!(inspect.state.running);
        assert_eq!(inspect.state.status, "running");

        Ok(Container {
            id: id.to_string(),
            delete_on_drop,
            inspect,
        })
    }

    pub fn exec_with_options(&self, options: ExecOptions, args: &[&str]) -> Result<Output> {
        if !options.capture.unwrap_or(false) {
            println_command!(args.join(" "));
        }

        let mut arguments = vec!["exec"];
        let opt_args = options.to_args();
        arguments.append(&mut opt_args.iter().map(|o| o.as_str()).collect());
        arguments.push(&self.id);
        arguments.extend_from_slice(args);

        Docker::cli(&arguments, options.capture.unwrap_or(false))
    }

    ///
    /// Executes a command inside the container with default options,
    /// e.g not hiding the command
    ///
    pub fn exec(&self, args: &[&str]) -> Result<Output> {
        self.exec_with_options(ExecOptions::default(), args)
    }

    pub fn copy_into(&self, input_path: &str, container_path: &str) -> Result<Output> {
        Docker::cli_output(&[
            "cp",
            input_path,
            &format!("{}:{}", self.id, container_path),
        ])
    }

    pub fn image(&self) -> &String {
        &self.inspect.config.image
    }

    pub fn id(&self) -> &String {
        &self.id
    }

    ///
    /// Returns the data of docker inspect
    ///
    pub fn inspect(&self) -> &DockerInspect {
        &self.inspect
    }

    pub fn stop(&self) -> Result<Output> {
        Docker::cli_capture(&["stop", &self.id])
    }

    pub fn rm(&self) -> Result<Output> {
        Docker::cli_capture(&["rm", &self.id])
    }
}

impl Drop for Container {
    fn drop(&mut self) {
        // Always stop containers
        println_status!("Stopping container…");
        if let Ok(ret) = self.stop() {
            if !ret.status.success() {
                eprintln!("Failed to stop container {}", &self.id);
                eprintln!("Error: {}", String::from_utf8(ret.stderr).unwrap());
                eprintln!("Please clean it up manually");
            }
        }

        // Only delete if wanted
        if self.delete_on_drop {
            println_status!("Deleting container…");
            if let Ok(ret) = self.rm() {
                if !ret.status.success() {
                    eprintln!("Failed to remove container {}", &self.id);
                    eprintln!("Error: {}", String::from_utf8(ret.stderr).unwrap());
                    eprintln!("Please clean it up manually");
                }
            }
        }
    }
}
