// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-only

use std::ops::{Deref, DerefMut};
use std::sync::Arc;
use std::sync::Mutex;
use std::{collections::HashMap, sync::MutexGuard};

use directories::ProjectDirs;

use lazy_static::lazy_static;

///
/// Data structure that represents a container in the configuration file
///
#[derive(Serialize, Deserialize, Debug)]
pub struct Container {
    pub id: String,
}

///
/// Struct that represents the configuration file
///
#[derive(Serialize, Deserialize, Default, Debug)]
pub struct Config {
    pub build_container: HashMap<String, Container>,
    pub distcc_container: Option<Container>,
}

lazy_static! {
    ///
    /// Global config object
    ///
    /// The global config object doesn't automatically save the config on exit.
    /// Make sure to call save before the application quits!
    ///
    static ref GLOBAL: Arc<Mutex<Config>> = Arc::from(Mutex::from(Config::open()));
}

impl Config {
    ///
    /// Open the config for the current scope.
    /// Once the Config object is being dropped, the config is automatically saved.
    /// This means that you need to make sure that Config::open
    /// is not called while a Config object is still in scope, otherwise changes might be overwritten.
    ///
    /// Use
    /// ```
    /// drop(config)
    /// ```
    /// to save and close the config before the scope ends.
    fn open() -> Config {
        if let Some(dirs) = ProjectDirs::from("io", "debian-pm", "package-builder") {
            let config_path = dirs.config_dir().join("config.json");
            let config: Config =
                serde_json::from_str(&std::fs::read_to_string(config_path).unwrap_or_default())
                    .unwrap_or_default();
            return config;
        }

        Config::default()
    }

    fn save(&self) {
        if let Some(dirs) = ProjectDirs::from("io", "debian-pm", "package-builder") {
            drop(std::fs::create_dir_all(dirs.config_dir()));
            let config_path = dirs.config_dir().join("config.json");
            drop(std::fs::write(
                config_path,
                serde_json::to_string_pretty(self).unwrap(),
            ));
        }
    }

    ///
    /// Get access to the global config object.
    /// Changes will automatically be saved when the ConfigGuard object goes out of scope
    ///
    pub fn global() -> ConfigGuard {
        ConfigGuard::new(&GLOBAL)
    }
}

///
/// ConfigGuard is a reference to the global config object.
/// It automatically saves the config when it goes out of scope.
/// This way the config can be automatically saved without reading and writing the config all the time,
/// with all the problems like overwriting data.
///
pub struct ConfigGuard {
    guard: MutexGuard<'static, Config>,
    dirty: bool,
}

impl ConfigGuard {
    fn new(config_ptr: &'static Arc<Mutex<Config>>) -> ConfigGuard {
        ConfigGuard {
            guard: config_ptr
                .try_lock()
                .expect("Two ConfigGuards can't exist at the same time"),
            dirty: false,
        }
    }
}

impl Deref for ConfigGuard {
    type Target = MutexGuard<'static, Config>;

    fn deref(&self) -> &Self::Target {
        &self.guard
    }
}

impl DerefMut for ConfigGuard {
    fn deref_mut(&mut self) -> &mut Self::Target {
        self.dirty = true;
        &mut self.guard
    }
}

impl Drop for ConfigGuard {
    fn drop(&mut self) {
        if self.dirty {
            //println!("### Saving config!");
            self.guard.save()
        }
    }
}
