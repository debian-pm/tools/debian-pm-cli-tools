// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-only

use crate::config::{self, Config};
use crate::container::{check, CommandResult, Container, ExecOptions, RunOptions};
use crate::distcc::Distcc;
use crate::io_check;
use crate::packageindex::PackageIndex;
use crate::utils::remove_duplicates;

use std::io::Result;
use std::process::Output;

use package_scanner::scan;

use users::get_current_uid;

use crate::println_status;

const CONTAINER_PKG_DIR: &str = "/packages/";
const DISTCC_WRAPPER_TMP: &str = "/tmp/distcc_wrapper";
const CONTAINER_DISTCC_WRAPPER: &str = "/usr/lib/distcc/distccwrapper";
const CONTAINER_CCACHE_DIR: &str = "/ccache";

fn extract_triplet(container: &Container) -> String {
    String::from_utf8_lossy(
        &container
            .exec_with_options(
                ExecOptions::default().capture(true),
                &["dpkg-architecture", "-qDEB_HOST_GNU_TYPE"],
            )
            .unwrap()
            .stdout,
    )
    .trim()
    .to_string()
}

fn build_jobs() -> u16 {
    // The number of CPUs + slightly more to account for IO overhead of distcc
    (num_cpus::get() as f32 * 1.2) as u16
}

pub struct PackageBuilder {
    build_container: Container,
    basepath: String,
    distcc: Distcc,
    triplet: String,
}

impl PackageBuilder {
    ///
    /// Returns the path to a ccache location outside the container.
    /// The path is guarenteed to always exist
    ///
    fn ccache_dir() -> String {
        let ccache_dir = directories::BaseDirs::new()
            .map(|basedirs| basedirs.home_dir().to_string_lossy().to_string())
            .unwrap_or_else(|| "~/".to_string())
            + "/.ccache";

        drop(std::fs::create_dir(&ccache_dir)); // How to make sure we really return an existing directory?

        ccache_dir
    }

    ///
    /// Start the contaier in which the builds will be run
    ///
    pub fn start(build_image: &str, architecture: &str, basepath: &str) -> Result<PackageBuilder> {
        {
            let config = Config::global();
            if let Some(build_container) = config.build_container.get(architecture) {
                if let Ok(build_container) = Container::from_existing_id(&build_container.id, false)
                {
                    if build_container.image() != build_image {
                        eprintln!(
                            "Switching the build container image is not supported right now."
                        );
                        eprintln!("Please delete the old container or the config file at ~/.config/package-builder/config.json");
                    }

                    assert_eq!(build_container.image(), build_image);

                    // ConfigGuard locks a mutex,
                    // and distcc also uses it, so we can't have to ConfigGuards at the same time
                    drop(config);

                    return Ok(PackageBuilder {
                        distcc: Distcc::start(&build_container, architecture)?,
                        triplet: extract_triplet(&build_container),
                        build_container,
                        basepath: basepath.to_string(),
                    });
                }
            }
        }

        let build_container = Container::start_with_options(
            build_image,
            RunOptions::default()
                .mount(basepath, CONTAINER_PKG_DIR)
                .mount(&PackageBuilder::ccache_dir(), CONTAINER_CCACHE_DIR)
                .delete_on_drop(false),
        )?;

        // Initial scan, so apt doesn't fail when updating
        scan(basepath, true);

        std::fs::write(
            "/tmp/local.list",
            format!("deb [trusted=yes] file:{} ./", CONTAINER_PKG_DIR),
        )
        .expect("Failed to write source list");
        build_container
            .copy_into("/tmp/local.list", "/etc/apt/sources.list.d/local.list")
            .expect("Failed to copy sources list to container");

        io_check!(build_container.exec_with_options(
            ExecOptions::default().user("root"),
            &["apt-get", "update"]
        ));
        io_check!(build_container.exec_with_options(
            ExecOptions::default().user("root"),
            &[
                "apt-get",
                "install",
                "-y",
                "build-essential",
                "devscripts",
                "distcc",
                "ccache"
            ]
        ));

        // Create distcc wrapper

        let triplet = extract_triplet(&build_container);
        let distcc_wrapper = &format!(
            r#"#!/usr/bin/env bash
exec /usr/lib/distcc/{}-g${{0:$[-2]}} "$@"
"#,
            triplet
        );

        std::fs::write(DISTCC_WRAPPER_TMP, distcc_wrapper)?;
        build_container.copy_into(DISTCC_WRAPPER_TMP, CONTAINER_DISTCC_WRAPPER)?;
        build_container.exec(&["chmod", "+x", CONTAINER_DISTCC_WRAPPER])?;

        let command_output = |command: &[&str]| {
            Result::<String>::Ok(
                String::from_utf8_lossy(
                    &build_container
                        .exec_with_options(ExecOptions::default().capture(true), command)?
                        .stdout,
                )
                .to_string(),
            )
        };

        // Remove existing non-complete distcc links
        let output = command_output(&["ls", "/usr/lib/distcc/"])?;
        let compilers_to_replace: Vec<&str> = output
            .trim()
            .split('\n')
            .filter(|c| !c.contains(&triplet) && !c.contains("distccwrapper"))
            .collect();

        println!("Replacing compilers: {:?}", compilers_to_replace);

        for bin in &compilers_to_replace {
            build_container.exec_with_options(
                ExecOptions::default().user("root"),
                &["rm", &format!("/usr/lib/distcc/{}", bin)],
            )?;
        }

        // Create new links
        for bin in &compilers_to_replace {
            build_container.exec_with_options(
                ExecOptions::default().user("root"),
                &[
                    "ln",
                    "-s",
                    CONTAINER_DISTCC_WRAPPER,
                    &format!("/usr/lib/distcc/{}", bin),
                ],
            )?;
        }

        // Add additional distcc symlinks
        io_check!(build_container.exec_with_options(
            ExecOptions::default().user("root"),
            &["ln", "-f", CONTAINER_DISTCC_WRAPPER, "/usr/lib/distcc/c++"]
        ));
        io_check!(build_container.exec_with_options(
            ExecOptions::default().user("root"),
            &["ln", "-f", CONTAINER_DISTCC_WRAPPER, "/usr/lib/distcc/cc"]
        ));

        {
            // Store new container id in config
            Config::global().build_container.insert(
                architecture.to_string(),
                config::Container {
                    id: build_container.id().to_string(),
                },
            );
        }

        Ok(PackageBuilder {
            distcc: Distcc::start(&build_container, architecture)?,
            build_container,
            basepath: basepath.to_string(),
            triplet: triplet.to_string(),
        })
    }

    ///
    /// Runs a command in a workdir
    ///
    /// The command will be executed as root
    fn run_workdir_command(&self, workdir: &str, command: &[&str]) -> Result<Output> {
        self.build_container.exec_with_options(
            ExecOptions::default().workdir(workdir).user("root"),
            command,
        )
    }

    ///
    /// Run build command in a workdir.
    /// This function sets all the necessary environment variables to use distcc
    /// for executing the build command, in case it calls a compiler.
    ///
    /// The build command will be executed as the current user, not as root
    fn run_build_command(&self, workdir: &str, command: &[&str]) -> Result<Output> {
        self.build_container.exec_with_options(
            ExecOptions::default()
                .workdir(workdir)
                .user(&get_current_uid().to_string())
                .env("DISTCC_HOSTS", &format!("{}/{}", self.distcc.ip_address(), build_jobs()))
                // I'm not 100% convinced hardoding the path will always work,
                // but doing this dynamically would also be messy
                .env(
                    "PATH",
                    "/usr/lib/ccache/:/usr/lib/distcc:/usr/local/bin:/usr/bin:/bin:/usr/local/games:/usr/games",
                )
                .env("HOME", "/tmp/")
                .env("CCACHE_DIR", CONTAINER_CCACHE_DIR)
                // Only compile in the distcc container
                //.env("DISTCC_FALLBACK", "0")
                // Compile with full compiler name
                // Avoids going through distccwrapper
                .env("CXX", &format!("{}-g++", self.triplet))
                .env("CC", &format!("{}-gcc", self.triplet)),
            command,
        )
    }

    ///
    /// Build a single package
    ///
    pub fn build_package(&self, index: &PackageIndex, source_package: &str) -> CommandResult {
        println_status!("Building {}", source_package);

        let container_workdir = CONTAINER_PKG_DIR.to_owned()
            + index
                .package_directory(source_package)
                .unwrap_or_else(|| panic!("Package {} not found", source_package))
                .to_str()
                .unwrap();

        // Allowed to fail if there are no files to delete, don't check
        drop(self.run_build_command(&container_workdir, &["bash", "-c", "rm ../*.asc"])); // .tar.asc files confuse origtargz

        // Check if each build command exited successfully
        check(self.run_workdir_command(&container_workdir, &["origtargz", "--clean"]))?;
        check(self.run_build_command(&container_workdir, &["origtargz"]))?;
        check(self.run_workdir_command(&container_workdir, &["apt-get", "update"]))?;
        check(
            self.run_workdir_command(&container_workdir, &["apt-get", "build-dep", ".", "-y"]),
        )?;

        // Not required to suceed
        drop(self.run_workdir_command(&container_workdir, &["apt-get", "clean"]));

        let n = build_jobs();
        check(self.run_build_command(&container_workdir, &["dpkg-buildpackage", &format!("-J{}", n)]))?;

        // Scan for new package files
        scan(&self.basepath, true);

        Ok(())
    }

    ///
    /// Build a single package and its dependencies
    ///
    pub fn build_package_with_dependencies(
        &self,
        index: &PackageIndex,
        package_name: &str,
    ) -> CommandResult {
        let buildorder = index.build_and_runtime_dependencies_buildorder(package_name);
        for package in buildorder {
            self.build_package(index, &package)?
        }

        Ok(())
    }

    ///
    /// Build multiple packages and their dependencies
    ///
    pub fn build_packages_with_dependencies(
        &self,
        index: &PackageIndex,
        package_names: Vec<String>,
    ) -> CommandResult {
        let buildorder: Vec<String> = remove_duplicates(
            package_names
                .iter()
                .flat_map(|p| index.build_and_runtime_dependencies_buildorder(p))
                .collect(),
        );

        for package in buildorder {
            self.build_package(index, &package)?;
        }

        Ok(())
    }
}
