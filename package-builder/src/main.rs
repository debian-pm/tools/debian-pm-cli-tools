// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-only

extern crate debian;
extern crate debos_parser;
extern crate directories;
extern crate lazy_static;
extern crate walkdir;

#[macro_use]
extern crate serde;

/// Configuration file
mod config;
/// Docker container API
mod container;
/// Debos Recipe
mod debosrecipe;
/// Distcc Container
mod distcc;
/// Docker image mapping
mod images;
/// Logging helpers
mod log;
/// Package Builder
mod package_builder;
/// Package index
mod packageindex;
/// reusable utility functions
mod utils;

use packageindex::PackageIndex;

use clap::{Command, Arg, ArgMatches, ArgAction};

use std::collections::HashMap;
use std::path::PathBuf;

use crate::config::Config;
use crate::container::Docker;

///
/// extract debos template variables from clap command line arguments
///
fn parse_debos_values(subcommand_values: &ArgMatches) -> HashMap<String, String> {
    let mut values = HashMap::<String, String>::new();
    if let Some(variables) = subcommand_values.get_many::<String>("variable") {
        for var in variables {
            let keyval: Vec<&str> = var.split(':').collect();
            let (key, value) = (keyval[0], keyval[1]);
            values.insert(key.to_string(), value.to_string());
        }
    }

    values
}

fn main() {
    let app = Command::new("Package builder")
        .version("1.0")
        .author("Jonah Brüchert <jbb@kaidan.im>")
        .about("builds packages in the correct order")
        .arg(
            Arg::new("basepath")
                .long("basepath")
                .value_name("basepath")
                .help("Path to find packages in. The path will be recursively searched for packages, up to a depth of 5")
                .action(ArgAction::Set)
                .required(true)
        )
        .subcommand(
            Command::new("buildorder")
                .about("Print buildorder of packages")
                .arg(
                    Arg::new("recipe")
                        .long("recipe")
                        .value_name("recipe")
                        .action(ArgAction::Set)
                        .required_unless_present("package"),
                )
                .arg(
                    Arg::new("variable")
                        .long("variable")
                        .short('t')
                        .value_name("variable")
                        .action(ArgAction::Append)
                )
                .arg(
                    Arg::new("package")
                        .long("package")
                        .value_name("package")
                        .action(ArgAction::Set)
                        .required_unless_present("recipe"),
                ),
        )
        .subcommand(
            Command::new("bootstrap")
                .about("Build packages")
                .arg(
                    Arg::new("recipe")
                        .long("recipe")
                        .value_name("recipe")
                        .action(ArgAction::Set)
                        .required_unless_present("package"),
                )
                .arg(
                    Arg::new("variable")
                        .long("variable")
                        .short('t')
                        .value_name("variable")
                        .action(ArgAction::Set)
                )
                .arg(
                    Arg::new("package")
                        .long("package")
                        .value_name("package")
                        .action(ArgAction::Set)
                        .required_unless_present("recipe"),
                ).arg(
                    Arg::new("architecture")
                        .long("architecture")
                        .value_name("architecture")
                        .short('a')
                        .action(ArgAction::Set)
                        .required_unless_present("recipe")
                ).arg(
                    Arg::new("distribution")
                        .long("distribution")
                        .value_name("distribution")
                        .short('d')
                        .default_value("testing")
                )
        )
        .subcommand(
            Command::new("build")
            .about("Build packages")
            .arg(
                Arg::new("package")
                    .long("package")
                    .value_name("package")
                    .action(ArgAction::Set)
                    .required(true),
            ).arg(
                Arg::new("architecture")
                    .long("architecture")
                    .value_name("architecture")
                    .short('a')
                    .action(ArgAction::Set)
                    .required(true)
            ).arg(
                Arg::new("distribution")
                    .long("distribution")
                    .value_name("distribution")
                    .short('d')
                    .default_value("testing")
            )
        ).subcommand(
            Command::new("prune")
                .about("Delete all containers")
        );

    let matches = app.get_matches();
    let basepath = matches.get_one::<String>("basepath").unwrap(); // unwrap is fine, argument required
    let basepath = std::fs::canonicalize(basepath).expect("Basepath invalid");
    let basepath = basepath.to_str().unwrap();

    // Set up Ctrl + C handler
    ctrlc::set_handler(move || {
        // Properly drop everything on the stack
    })
    .expect("Error setting Ctrl-C handler");

    if matches.subcommand_matches("prune").is_some() {
        let mut config = Config::global();
        for container in config.build_container.values() {
            Docker::cli_output(&["rm", &container.id])
                .expect("Failed to remove build container");
        }
        config.build_container.clear();

        if let Some(distcc_container) = &config.distcc_container {
            Docker::cli_output(&["rm", &distcc_container.id])
                .expect("Failed to remove distcc container");
        }
        config.distcc_container = None;
    } else {
        // Index packages
        let index = PackageIndex::generate(basepath);

        if let Some(buildorder) = matches.subcommand_matches("buildorder") {
            if let Some(debosrecipe) = buildorder.get_one::<String>("recipe") {
                let recipe = debosrecipe::expanded_recipe(
                    PathBuf::from(debosrecipe),
                    parse_debos_values(buildorder),
                )
                .unwrap();

                println!(
                    "{:?}",
                    &index.multiple_build_and_runtime_dependencies_buildorder(
                        debosrecipe::get_used_packages(&recipe)
                    )
                );
            } else if let Some(package_name) = buildorder.get_one::<String>("package") {
                println!(
                    "{:?}",
                    index.build_and_runtime_dependencies_buildorder(package_name)
                );
            }
        }

        if let Some(bootstrap) = matches.subcommand_matches("bootstrap") {
            if let Some(debosrecipe) = bootstrap.get_one::<String>("recipe") {
                if let Ok(recipe) = debosrecipe::expanded_recipe(
                    PathBuf::from(debosrecipe),
                    parse_debos_values(bootstrap),
                ) {
                    let package_builder = crate::package_builder::PackageBuilder::start(
                        &images::build_image_for_architecture(
                            recipe.architecture(),
                            bootstrap.get_one::<String>("distribution").unwrap_or(&"testing".to_string()),
                        )
                        .unwrap(),
                        recipe.architecture(),
                        basepath,
                    )
                    .unwrap();

                    package_builder
                        .build_packages_with_dependencies(
                            &index,
                            debosrecipe::get_used_packages(&recipe),
                        )
                        .unwrap();
                }
            }

            if let Some(package_name) = bootstrap.get_one::<String>("package") {
                let architecture = bootstrap
                    .get_one::<String>("architecture")
                    .expect("architecture argument not given although it is mandatory");
                let package_builder = crate::package_builder::PackageBuilder::start(
                    &images::build_image_for_architecture(
                        architecture,
                        bootstrap.get_one::<String>("distribution").unwrap(),
                    )
                    .unwrap(),
                    architecture,
                    basepath,
                )
                .unwrap();

                package_builder
                    .build_package_with_dependencies(&index, package_name)
                    .unwrap();
            }
        }

        if let Some(build) = matches.subcommand_matches("build") {
            let package = build.get_one::<String>("package").unwrap();
            let architecture = build
                .get_one::<String>("architecture")
                .expect("architecture argument not given although it is mandatory");
            let package_builder = crate::package_builder::PackageBuilder::start(
                &images::build_image_for_architecture(
                    architecture,
                    build.get_one::<String>("distribution").unwrap(),
                )
                .unwrap(),
                architecture,
                basepath,
            )
            .unwrap();

            package_builder.build_package(&index, package).unwrap();
        }
    }
}
