// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-only

use crate::utils::remove_duplicates;

use std::collections::HashMap;
use std::fs;
use std::path::Path;
use std::path::PathBuf;

use debian::package::ControlFile;
use walkdir::WalkDir;

use crate::println_status;

#[derive(Serialize, Deserialize)]
pub struct PackageIndex {
    build_dependencies: HashMap<String, Vec<String>>,
    dependencies: HashMap<String, Vec<String>>,
    bin2source: HashMap<String, String>,
    directories: HashMap<String, PathBuf>,
}

impl PackageIndex {
    ///
    /// Generate a new index for the specified path
    ///
    /// The directory will be scanned for debian/control files up to a depth of 5
    ///
    pub fn generate(basepath: &str) -> PackageIndex {
        fn read_dependency_fields(path: &Path, field: &str) -> Option<(String, Vec<String>)> {
            match ControlFile::from_file(path) {
                Ok(control) => {
                    let paragraphs = control.get_paragraphs();
                    let package = paragraphs.first()?.get_entry("Source")?.to_string();
                    let dependencies = paragraphs
                        .iter()
                        .flat_map(|paragraph| match paragraph.get_entry(field) {
                            Some(deps) => {
                                debian::package::parse_dep_list(deps)
                                    .unwrap_or_else(|_| panic!("Failed to parse dependency list of {}",
                                        package))
                                    .into_iter()
                                    .flat_map(|d| d.alternatives.into_iter().map(|a| a.package))
                                    .collect()
                            }
                            None => Vec::new(),
                        })
                        .collect();
                    Some((package, dependencies))
                }
                Err(error) => {
                    println!("{}", error);
                    None
                }
            }
        }

        fn read_build_dependencies(path: &Path) -> Option<(String, Vec<String>)> {
            read_dependency_fields(path, "Build-Depends")
        }

        fn read_dependencies(path: &Path) -> Option<(String, Vec<String>)> {
            read_dependency_fields(path, "Depends")
        }

        fn read_bin2source(path: &Path) -> Option<(Vec<String>, String)> {
            match ControlFile::from_file(path) {
                Ok(control) => {
                    let paragraphs = control.get_paragraphs();
                    let bin_packages: Vec<String> = paragraphs
                        .iter()
                        .filter_map(|p| p.get_entry("Package"))
                        .map(|p| p.to_string())
                        .collect();
                    let source = paragraphs
                        .first()
                        .unwrap()
                        .get_entry("Source")
                        .unwrap()
                        .to_string();

                    Some((bin_packages, source))
                }
                Err(e) => {
                    eprintln!("Failed to parse {} file {}", path.to_str().unwrap(), e);
                    None
                }
            }
        }

        fn read_source_name(path: &Path) -> Option<String> {
            match ControlFile::from_file(path) {
                Ok(control) => Some(
                    control
                        .get_paragraphs()
                        .first()
                        .unwrap()
                        .get_entry("Source")
                        .unwrap()
                        .to_string(),
                ),
                Err(e) => {
                    eprintln!("Failed to parse {} file {}", path.to_str().unwrap(), e);
                    None
                }
            }
        }

        println_status!("Indexing packages...");
        let mut build_dependencies: HashMap<String, Vec<String>> = HashMap::new();
        let mut dependencies: HashMap<String, Vec<String>> = HashMap::new();
        let mut bin2source: HashMap<String, String> = HashMap::new();
        let mut directories: HashMap<String, PathBuf> = HashMap::new();

        for entry in WalkDir::new(basepath)
            .follow_links(true)
            .max_depth(5)
            .into_iter()
            .filter_entry(|e| {
                e.file_name()
                    .to_str()
                    .map(|s| !s.contains(".git"))
                    .unwrap_or(true)
            })
            .filter_map(|e| e.ok())
        {
            let parent: &Path = entry.path().parent().unwrap();
            if entry.file_name() == "control" && parent.ends_with("debian") {
                if let Some((package, bdeps)) = read_build_dependencies(entry.path()) {
                    build_dependencies.insert(package, bdeps);
                }
                if let Some((package, deps)) = read_dependencies(entry.path()) {
                    dependencies.insert(package, deps);
                }
                if let Some((bin_packages, source)) = read_bin2source(entry.path()) {
                    for bin in bin_packages {
                        bin2source.insert(bin, source.clone());
                    }
                }
                if let Some(name) = read_source_name(entry.path()) {
                    if let Some(absolute_path) =
                        entry.path().parent().unwrap().parent().unwrap().to_str()
                    {
                        let absolute_basepath =
                            fs::canonicalize(PathBuf::from(basepath.to_owned())).unwrap();
                        let relative_path =
                            absolute_path.replace(absolute_basepath.to_str().unwrap(), "");
                        directories.insert(name, PathBuf::from(relative_path));
                    }
                }
            }
        }

        PackageIndex {
            build_dependencies,
            dependencies,
            bin2source,
            directories,
        }
    }

    ///
    /// Returns the source package that a binary is built from
    ///
    pub fn source_of_binary(&self, bin_package: &str) -> Option<&String> {
        self.bin2source.get(bin_package)
    }

    ///
    /// Returns the build dependencies of a source package
    ///
    pub fn build_dependencies(&self, source_package: &str) -> Option<&Vec<String>> {
        self.build_dependencies.get(source_package)
    }

    pub fn dependencies(&self, source_package: &str) -> Option<&Vec<String>> {
        self.dependencies.get(source_package)
    }

    ///
    /// Returns the order in which all dependencies of a source package need to be built
    ///
    pub fn build_dependencies_buildorder(&self, source_package: &str) -> Vec<String> {
        if !self.build_dependencies.contains_key(source_package) {
            return Vec::new();
        }

        let deps = self.build_dependencies(source_package).unwrap().clone();

        // Add recursive dependencies
        let mut new_deps = Vec::<String>::new();
        for dep in &deps {
            if let Some(dsource_package) = self.source_of_binary(dep) {
                // Catch direct depdenency loops (looking at you qtbase)
                if dsource_package != source_package {
                    for rdep in self.build_dependencies_buildorder(dsource_package) {
                        // Don't add dependencies twice
                        if !new_deps.contains(&rdep) {
                            new_deps.push(rdep);
                        }
                    }
                }
            }
        }

        new_deps.push(source_package.to_string());

        new_deps
    }

    pub fn build_and_runtime_dependencies_buildorder(&self, source_package: &str) -> Vec<String> {
        if !self.dependencies.contains_key(source_package) {
            return Vec::new();
        }

        let mut build_deps = self.build_dependencies_buildorder(source_package);
        if let Some(dependencies) = self.dependencies(source_package) {
            let mut dependencies: Vec<String> = dependencies
                .iter()
                .flat_map(|d| {
                    if let Some(dsource) = self.source_of_binary(d) {
                        self.build_dependencies_buildorder(dsource)
                    } else {
                        Vec::new()
                    }
                })
                .collect();

            dependencies.append(&mut build_deps);
            let dependencies = remove_duplicates(dependencies);
            return dependencies;
        }

        build_deps
    }

    pub fn multiple_build_and_runtime_dependencies_buildorder(
        &self,
        source_packages: Vec<String>,
    ) -> Vec<String> {
        remove_duplicates(
            source_packages
                .iter()
                .filter_map(|p| self.source_of_binary(p))
                .flat_map(|p| self.build_and_runtime_dependencies_buildorder(p))
                .collect(),
        )
    }

    pub fn package_directory(&self, source_package: &str) -> Option<&PathBuf> {
        self.directories.get(source_package)
    }
}

#[test]
fn test_remove_duplicates() {
    let duplicated_list = vec!["a", "b", "c", "d", "e", "a", "c", "f"];
    assert_eq!(
        remove_duplicates(duplicated_list),
        vec!["a", "b", "c", "d", "e", "f"]
    );
}

#[test]
fn test_build_dependencies() {
    let mut build_dependencies: HashMap<String, Vec<String>> = HashMap::new();
    build_dependencies.insert(
        "PACKAGE3".to_string(),
        vec!["package1".to_string(), "package2".to_string()],
    );
    build_dependencies.insert("PACKAGE2".to_string(), vec!["package1".to_string()]);
    build_dependencies.insert("PACKAGE1".to_string(), vec![]);

    let mut bin2source: HashMap<String, String> = HashMap::new();
    bin2source.insert("package1".to_string(), "PACKAGE1".to_string());
    bin2source.insert("package2".to_string(), "PACKAGE2".to_string());
    bin2source.insert("package3".to_string(), "PACKAGE3".to_string());
    let index = PackageIndex {
        bin2source,
        build_dependencies,
        dependencies: HashMap::new(),
        directories: HashMap::new(),
    };

    let buildorder = index.build_dependencies_buildorder("PACKAGE3");
    assert_eq!(buildorder, vec!["PACKAGE1", "PACKAGE2", "PACKAGE3"]);
}

#[test]
fn test_build_and_runtime_dependencies() {
    let mut build_dependencies: HashMap<String, Vec<String>> = HashMap::new();
    build_dependencies.insert(
        "PACKAGE3".to_string(),
        vec!["package1".to_string(), "package2".to_string()],
    );
    build_dependencies.insert("PACKAGE2".to_string(), vec!["package1".to_string()]);
    build_dependencies.insert("PACKAGE1".to_string(), vec![]);
    build_dependencies.insert("RUNTIME1".to_string(), vec!["package1".to_string()]);

    let mut bin2source: HashMap<String, String> = HashMap::new();
    bin2source.insert("package1".to_string(), "PACKAGE1".to_string());
    bin2source.insert("package2".to_string(), "PACKAGE2".to_string());
    bin2source.insert("package3".to_string(), "PACKAGE3".to_string());
    bin2source.insert("runtime1".to_string(), "RUNTIME1".to_string());

    let mut dependencies: HashMap<String, Vec<String>> = HashMap::new();
    dependencies.insert("PACKAGE3".to_string(), vec!["runtime1".to_string()]);
    let index = PackageIndex {
        bin2source,
        build_dependencies,
        dependencies,
        directories: HashMap::new(),
    };

    let buildorder = index.build_dependencies_buildorder("PACKAGE3");
    assert_eq!(buildorder, vec!["PACKAGE1", "PACKAGE2", "PACKAGE3"]);

    let buildorder = index.build_and_runtime_dependencies_buildorder("PACKAGE3");
    assert_eq!(
        buildorder,
        vec!["PACKAGE1", "RUNTIME1", "PACKAGE2", "PACKAGE3"]
    );
}
